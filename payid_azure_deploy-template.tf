# Configure the Azure Provider for PayID
provider "azurerm" {
  version = "~>2.20.0"
    use_msi = true
    subscription_id = "xxxxxxxxxxxxxxxxx"
    tenant_id       = "xxxxxxxxxxxxxxxxx"
    features {}
}

# Create a resource group for PayID
resource "azurerm_resource_group" "payid_azure_deploy-template" {
  name     = "payid_azure_deploy-template-resources"
  location = "eastus2"
}

# Create a virtual network within the resource group for PayID
resource "azurerm_virtual_network" "payid_azure_deploy-template" {
  name                = "payid_azure_deploy-template-network"
  resource_group_name = azurerm_resource_group.payid_azure_deploy-template.name
  location            = azurerm_resource_group.payid_azure_deploy-template.location
  address_space       = ["10.0.0.0/16"]
}