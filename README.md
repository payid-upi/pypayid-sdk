# PyPayID-SDK #



### What is PyPayID-SDK? ###

**PyPayID-SDK** is a software development kit for **PayID** that contains:

* documentation
* sample code
* extensions
* python APIs
* wallet templates
* **Terraform** deployment templates (**AWS**, **GCP**, **Azure**)
* analytical & monitoring tools
